# advanced https OTA example

First you configure the project via `make menuconfig`.
1. Serial flasher config - set up correct serial port for flashing example `/dev/ttyUSB0`
2. (if needed - set flash size - for OTA you need a flash size larger than 2M, eg. 4M)
3. Example Configuration: set WiFi ssid + password, firmware upgrade URL
4. Partition Table - select "Factory app, two OTA definitions" (Single factory app, no OTA doesn't work)

Generate SSL certificates for the server

*NOTE: `Common Name` of server certificate should be host-name of your server.*

```
openssl req -x509 -newkey rsa:2048 -keyout ca_key.pem -out ca_cert.pem -days 365

```

* openssl configuration may require you to enter a passphrase for the key.
* When prompted for the `Common Name (CN)`, enter the name of the server that the ESP32 will connect to. For this local example, it is probably the IP address. The HTTPS client will make sure that the `CN` matches the address given in the HTTPS URL.


Copy the certificate to `server_certs` directory inside OTA example directory.


Start the https server from the directory that has the file you want to serve (in other words 'build'):
```
openssl s_server -WWW -key ca_key.pem -cert ca_cert.pem -port 8070
```

To build the project and flash in one go use `make flash`.

App version number can be set like this
1. If ``PROJECT_VER`` variable set in project Cmake/Makefile file, its value will be used.
2. Else, if the ``$PROJECT_PATH/version.txt`` exists, its contents will be used as ``PROJECT_VER``.
3. Else, if the project is located inside a Git repository, the output of ``git describe`` will be used.
4. Otherwise, ``PROJECT_VER`` will be "1".

In ``native_ota_example``, ``$PROJECT_PATH/version.txt`` is used to define the version of app. Change the version in the file to compile the new firmware.

You can use `make monitor` from (any) example directory (if it has the correct serial config) to monitor the console output of the esp32.

Error messages from components can be cryptic. Check the simple things first, like correct file name and urls, and so on.