# ESP32 testing

Just testing the examples that comes with the [ESP-IDF](https://github.com/espressif/esp-idf) tool.

hello_world - nothing to change, works out of the box (ok, you need to set the correct usb serial port)

The OTA examples are from ESP-IDF's [examples/system/ota](https://github.com/espressif/esp-idf/tree/master/examples/system/ota), using that README as a guide.

simple_ota_example - works out of the box after setting the correct configuration, and using hello_world.bin as the payload.

native_ota_example - works out of the box after setting the correct configuration.

avdanced_https_ota - I had to change configuration for Partition Table; to set up OTA, and I had to change flash size from 2M to 4M in Serial flasher config. Then it works.